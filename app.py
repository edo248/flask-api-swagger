import flask

from marshmallow import Schema, fields as schema_field_type
from flask_apispec import use_kwargs, marshal_with, FlaskApiSpec, MethodResource
from flask_apispec.annotations import doc
from flask_restful import Resource as RestfulResource, Api

from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

# from .models import Pet
# from .schemas import PetSchema

POSTGRES = {
    'user': 'postgres',
    'pw': 'password',
    'db': 'binaries',
    'host': 'localhost',
    'port': '5999',
}

pg_connection_string = 'postgresql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES


app = flask.Flask(__name__)
app.config.update({
    'APISPEC_TITLE': "Library api",
    'APISPEC_VERSION': "v1",
    'APISPEC_SWAGGER_URL': '/spec.json',
    'APISPEC_SWAGGER_UI_URL' : '/docs/',
    'SQLALCHEMY_DATABASE_URI' : pg_connection_string,
    'SQLALCHEMY_TRACK_MODIFICATIONS' : False
})

swagger_docs = FlaskApiSpec(app)
api = Api(app)
db = SQLAlchemy(app)
ma = Marshmallow(app)

class Author(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))

class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    author_id = db.Column(db.Integer, db.ForeignKey('author.id'))
    author = db.relationship('Author', backref='books')


class AuthorSchema(ma.ModelSchema):
    id = schema_field_type.Int()
    name = schema_field_type.String()

class BookSchema(ma.ModelSchema):
    class Meta:
        model = Book

    author = ma.Nested(AuthorSchema, only=['name'])


def get_book(book_id):
    return Book.query.filter(Book.id == book_id).one()

def get_all_books():
    return Book.query.order_by(Book.id).all()


@doc(tags=['books'])
class BookResource(MethodResource):
    @marshal_with(BookSchema, code=200,
                  description="Book record")
    @doc(params={'id': {'description': 'Book id'}})
    def get(self, id):
        return get_book(id)

    @marshal_with(BookSchema, code=200,
                  description="Modified book record")
    @doc(params={'id': {'description': 'Book id'}})
    def put(self, id):
        book = get_book(id)
        return book


@doc(tags=['books'])
class BooksResource(MethodResource):
    @marshal_with(BookSchema(many=True),
                  code=200, description="List of all available books")
    def get(self):
        return get_all_books()

api.add_resource(BooksResource, '/book')
api.add_resource(BookResource, '/book/<id>')

swagger_docs.register(BooksResource)
swagger_docs.register(BookResource)

def test_db_init():
     db.create_all()
     author_schema = AuthorSchema()
     #author = Author(name='Chuck Paluhniuk')
    # db.session.add(author)
     # author = Author(name='Chuck Paluhniuk')
     # db.session.add(author)
     author = Author.query.filter(Author.id == 1).one()
     book = Book(title='Fight Club', author=author)
     db.session.add(book)
     db.session.commit()
     print(BookSchema().dump(book).data)

if __name__ == '__main__':
    #test_db_init()
    app.run(debug=True)